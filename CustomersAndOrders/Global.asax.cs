﻿using System.Web;
using System.Web.Http;

namespace CustomersAndOrders
{
	public class WebApiApplication : HttpApplication
	{
		protected void Application_Start()
		{
			GlobalConfiguration.Configure(WebApiConfig.Register);

			HttpConfiguration config = GlobalConfiguration.Configuration;

			config.Formatters.JsonFormatter.SerializerSettings
				.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
		}
	}
}
