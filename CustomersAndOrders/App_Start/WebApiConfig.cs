﻿using System.Web.Http;

namespace CustomersAndOrders
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			// Web API configuration and services

			// Web API routes
			config.MapHttpAttributeRoutes();

			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "V1/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);
		}
	}
}
