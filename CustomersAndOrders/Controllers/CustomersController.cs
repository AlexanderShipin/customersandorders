﻿using System.Collections.Generic;
using System.Web.Http;
using CustomersAndOrders.Models;
using CustomersAndOrders.Source;

namespace CustomersAndOrders.Controllers
{
	public class CustomersController : ApiController
	{
		private IRepository<Customer> _repo;

		public CustomersController()
		{
			_repo = Locator.GetRepository<Customer>(GetType(), new CustomersAndOrdersEntities());
		}

		public CustomersController(IRepository<Customer> repo)
		{
			_repo = repo;
		}

		public List<Customer> Get()
		{
			return _repo.Get();
		}

		public Customer Get(int id)
		{
			return _repo.Get(id);
		}

		public Order Post(int id, [FromBody]Order order)
		{
			var customer = new Customer();
			order.CreatedDate = order.CreatedDate.ToLocalTime();
			customer.Order.Add(order);
			_repo.Save(customer);
			return order;
		}

		public Customer Post([FromBody]Customer customer)
		{
			_repo.Save(customer);
			return customer;
		}
	}
}