﻿using System;
using System.Collections.Generic;
using CustomersAndOrders.Controllers;
using CustomersAndOrders.Models;

namespace CustomersAndOrders.Source
{
	public class Locator
	{
		private static Dictionary<Type, Type> _repositoryMapper = new Dictionary<Type, Type>();

		static Locator()
		{
			_repositoryMapper.Add(typeof(CustomersController), typeof(CustomerRepository));
		}

		public static IRepository<T> GetRepository<T>(Type controllerType, CustomersAndOrdersEntities context)
		{
			var inst = (IRepository<T>)Activator.CreateInstance(_repositoryMapper[controllerType], context);
			return inst;
		}
	}
}