﻿using System.Collections.Generic;

namespace CustomersAndOrders.Source
{
	public interface IRepository<T>
	{
		List<T> Get();
		T Get(int id);
		void Save(T value);
	}
}
