﻿using System.Collections.Generic;
using System.Linq;
using CustomersAndOrders.Models;

namespace CustomersAndOrders.Source
{
	public class DummyCustomerRepository : IRepository<Customer>
	{
		private List<Customer> _customerList;

		public DummyCustomerRepository()
		{
			_customerList = new List<Customer>();
			_customerList.Add(new Customer { Id = 1, Name = "TestUser1" });
			_customerList.Add(new Customer { Id = 2, Name = "TestUser2" });
			_customerList.Add(new Customer { Id = 3, Name = "TestUser3" });
		}

		public List<Customer> Get()
		{
			return _customerList;
		}

		public Customer Get(int id)
		{
			return _customerList.FirstOrDefault(c => c.Id == id);
		}

		public void Save(Customer value)
		{
		}
	}
}