﻿using System.Collections.Generic;
using System.Linq;
using CustomersAndOrders.Models;

namespace CustomersAndOrders.Source
{
	public class CustomerRepository : IRepository<Customer>
	{
		private CustomersAndOrdersEntities _dbContext;

		public CustomerRepository(CustomersAndOrdersEntities ctx)
		{
			_dbContext = ctx;
		}

		public List<Customer> Get()
		{
			var customers = _dbContext.Customer.ToList();
			return customers;
		}

		public Customer Get(int id)
		{
			var customer = _dbContext.Customer.FirstOrDefault(c => c.Id == id);
			return customer;
		}

		public void Save(Customer customer)
		{
			var order = customer.Order.FirstOrDefault();
			if (order == null)
				_dbContext.Customer.Add(customer);
			else
				_dbContext.Order.Add(order);

			_dbContext.SaveChanges();
		}
	}
}