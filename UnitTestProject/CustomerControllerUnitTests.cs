﻿using System;
using CustomersAndOrders.Controllers;
using CustomersAndOrders.Models;
using CustomersAndOrders.Source;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
	[TestClass]
	public class CustomerControllerUnitTests
	{
		[TestMethod]
		public void CustomerController_Get_CustomersCountIsCorrect()
		{
			// Arrange
			var controller = new CustomersController(new DummyCustomerRepository());

			// Act
			var customers = controller.Get();

			// Assert
			Assert.AreEqual(3, customers.Count);
		}

		[TestMethod]
		public void CustomerController_GetById_CustomersIsCorrect()
		{
			// Arrange
			var controller = new CustomersController(new DummyCustomerRepository());

			// Act
			var customer = controller.Get(1);
			
			// Assert
			Assert.AreEqual(1, customer.Id);
		}

		[TestMethod]
		public void CustomerController_Post_OrderCreated()
		{
			// Arrange
			var controller = new CustomersController(new DummyCustomerRepository());
			var newOrder = new Order {Id = 1, CustomerId = 1, Price = 10, CreatedDate = new DateTime()};

			// Act
			var order = controller.Post(1, newOrder);

			// Assert
			Assert.AreEqual(1, order.Id);
		}

		[TestMethod]
		public void CustomerController_Post_CustomerCreated()
		{
			// Arrange
			var controller = new CustomersController(new DummyCustomerRepository());
			var newCustomer = new Customer {Id = 10, Name = "TestName10", Email = "TestEmail"};

			// Act
			var customer = controller.Post(newCustomer);

			// Assert
			Assert.AreEqual(10, customer.Id);
		}
	}
}
