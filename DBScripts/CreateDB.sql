/*Create database, tables and test values*/
USE [master]
GO

/*Create database*/
CREATE DATABASE [CustomersAndOrders]
GO

USE [CustomersAndOrders]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*Create tables*/
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CustomersAndOrders]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Price] [decimal](12, 2) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO

ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Customer]
GO

/*Insert values*/
/*Insert values to Customer table*/
INSERT INTO [CustomersAndOrders].[dbo].[Customer]
           ([Name]
           ,[Email])
     VALUES
           (N'Frank Bryant'
           ,N'fbryant0@facebook.com')
GO
INSERT INTO [CustomersAndOrders].[dbo].[Customer]
           ([Name]
           ,[Email])
     VALUES
           (N'Harold Meyer'
           ,N'hmeyer1@indiatimes.com')
GO
INSERT INTO [CustomersAndOrders].[dbo].[Customer]
           ([Name]
           ,[Email])
     VALUES
           (N'Marilyn Schmidt'
           ,N'mschmidt2@amazon.de')
GO
INSERT INTO [CustomersAndOrders].[dbo].[Customer]
           ([Name]
           ,[Email])
     VALUES
           (N'Patrick Marshall'
           ,N'pmarshall3@sourceforge.net')
GO
INSERT INTO [CustomersAndOrders].[dbo].[Customer]
           ([Name]
           ,[Email])
     VALUES
           (N'Sara Arnold'
           ,N'sarnold4@360.cn')
GO
INSERT INTO [CustomersAndOrders].[dbo].[Customer]
           ([Name]
           ,[Email])
     VALUES
           (N'VeryLongCustomerNameForTestUiVeryLongCustomerNameF'
           ,N'VeryLongEmailForTestUIVeryLon@verylonghostname.com')
GO

/*Insert values to Order table*/
INSERT INTO [CustomersAndOrders].[dbo].[Order]
           ([Price]
           ,[CreatedDate]
           ,[CustomerId])
     VALUES
           (10
           ,'20160201'
           ,1)
GO
INSERT INTO [CustomersAndOrders].[dbo].[Order]
           ([Price]
           ,[CreatedDate]
           ,[CustomerId])
     VALUES
           (55.50
           ,'20160201'
           ,2)
GO
INSERT INTO [CustomersAndOrders].[dbo].[Order]
           ([Price]
           ,[CreatedDate]
           ,[CustomerId])
     VALUES
           (12
           ,'20160202'
           ,2)
GO
INSERT INTO [CustomersAndOrders].[dbo].[Order]
           ([Price]
           ,[CreatedDate]
           ,[CustomerId])
     VALUES
           (10
           ,'20160201'
           ,3)
GO
INSERT INTO [CustomersAndOrders].[dbo].[Order]
           ([Price]
           ,[CreatedDate]
           ,[CustomerId])
     VALUES
           (120
           ,'20160205'
           ,3)
GO
INSERT INTO [CustomersAndOrders].[dbo].[Order]
           ([Price]
           ,[CreatedDate]
           ,[CustomerId])
     VALUES
           (17
           ,'20160206'
           ,3)
GO
INSERT INTO [CustomersAndOrders].[dbo].[Order]
           ([Price]
           ,[CreatedDate]
           ,[CustomerId])
     VALUES
           (7
           ,'20160203'
           ,5)
GO
INSERT INTO [CustomersAndOrders].[dbo].[Order]
           ([Price]
           ,[CreatedDate]
           ,[CustomerId])
     VALUES
           (54.10
           ,'20160204'
           ,5)
GO
INSERT INTO [CustomersAndOrders].[dbo].[Order]
           ([Price]
           ,[CreatedDate]
           ,[CustomerId])
     VALUES
           (1234567890.12
           ,'20160204'
           ,6)
GO
